# Smart Date - Extra Tokens

Provides additional tokens to be used in conjunction with the
smart_date modules.

## Requirements

Requires the _smart_date_ module and _smart_date_recur_ sub-module.

## Install

Install module via composer:

```bash
composer require drupal/smart_date_extra_tokens
```

Enable via drush:
```bash
drush en smart_date_extra_tokens
```

## Configuration/Setup

No additional configuration is required or provided. See information
below regarding provided tokens and usage.

### Smart date formats

Can be managed here:

_/admin/config/regional/smart-date_

### Smart date custom tokens

Tokens provided on smart dates:

#### value-closest

Grabs the proper start date for smart dates/recurring fields and
formats correctly.

Example of token structure:
```
[<entity_type>:<field_name>:<delta>:value-closest:<date format>]
```

Example of token usage:
```
[node:field_when:0:value-closest:Y-m-d]
```

### end_value-closest

Grabs the closest end date for smart dates/recurring fields and
formats correctly.

Example of token structure:
```
[<entity_type>:<field_name>:<delta>:end_value-closest:<date format>]
```

Example of token usage:
```
[node:field_when:0:end_value-closest:Y-m-d]
```

## Maintainers

* George Anderson (geoanders) - https://drupal.org/u/geoanders
