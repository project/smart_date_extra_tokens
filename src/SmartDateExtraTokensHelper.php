<?php

namespace Drupal\smart_date_extra_tokens;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\smart_date\SmartDateTrait;

/**
 * Smart Date Extra Tokens Helper.
 */
class SmartDateExtraTokensHelper {

  use SmartDateTrait;

  /**
   * Helper function to find the closest instance.
   *
   * @param array $instances
   *   The smart date items.
   * @param string $prop_name
   *   The token/property name.
   *
   * @return int
   *   Returns the closest smart date item index.
   */
  public static function findClosestInstance(array $instances, string $prop_name): int {
    $closest_index = -1;
    $time = time();
    $current_dt = DrupalDateTime::createFromTimestamp($time);
    foreach ($instances as $index => $instance) {
      if (!empty($instance->$prop_name)) {
        $instance_dt = DrupalDateTime::createFromTimestamp($instance->$prop_name);
        if ($instance_dt && $current_dt) {
          if ($instance_dt->format('Ymd') == $current_dt->format('Ymd')) {
            $closest_index = $index;
            break;
          }
          if ($instance->$prop_name >= $time) {
            $closest_index = $index;
            break;
          }
        }
      }
    }
    return $closest_index;
  }

  /**
   * Format timestamp fetched from smart date item.
   *
   * @param string $timestamp
   *   The timestamp.
   * @param array $settings
   *   The settings.
   * @param string|null $timezone
   *   The timezone.
   *
   * @return string
   *   Returns the formatted timestamp.
   */
  public static function formatTimeStamp(string $timestamp, array $settings = [], string $timezone = NULL): string {
    /** @var \Drupal\Core\Datetime\DateFormatter $date_formatter */
    $date_formatter = \Drupal::service('date.formatter');
    $format = !empty($settings['date_format']) ? $settings['date_format'] : $settings['time_format'];

    return $date_formatter->format($timestamp, '', $format, $timezone);
  }

}
