<?php

/**
 * @file
 * Provides tokens for the smart_date_extra_tokens module.
 */

use Drupal\smart_date_extra_tokens\SmartDateExtraTokensHelper;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\smart_date\Plugin\Field\FieldType\SmartDateFieldItemList;
use Drupal\smart_date\Plugin\Field\FieldType\SmartDateItem;

/**
 * Implements hook_token_info().
 */
function smart_date_extra_tokens_token_info() {
  if (!\Drupal::hasService('token.entity_mapper')) {
    return;
  }

  $types = [];
  $tokens = [];
  foreach (\Drupal::entityTypeManager()
    ->getDefinitions() as $entity_type_id => $entity_type) {
    if (!$entity_type->entityClassImplements(ContentEntityInterface::class)) {
      continue;
    }

    $token_type = \Drupal::service('token.entity_mapper')
      ->getTokenTypeForEntityType($entity_type_id);
    if (empty($token_type)) {
      continue;
    }

    // Build custom property tokens for all smart date fields.
    $fields = \Drupal::service('entity_field.manager')
      ->getFieldStorageDefinitions($entity_type_id);
    foreach ($fields as $field_name => $field) {
      // Only want smart dates.
      if ($field->getType() != 'smartdate') {
        continue;
      }

      $tokens[$token_type . '-' . $field_name]['value-closest'] = [
        'name' => t('Start, closest instance with custom format'),
        'description' => t('Grabs the closest instance start date with custom formatting applied.'),
        'dynamic' => TRUE,
        'module' => 'smart_date_extra_tokens',
      ];

      $tokens[$token_type . '-' . $field_name]['end_value-closest'] = [
        'name' => t('End, closest instance with custom format'),
        'description' => t('Grabs the closest instance end date with custom formatted applied.'),
        'dynamic' => TRUE,
        'module' => 'smart_date_extra_tokens',
      ];
    }
  }

  return [
    'types' => $types,
    'tokens' => $tokens,
  ];
}

/**
 * Implements hook_tokens().
 */
function smart_date_extra_tokens_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  $replacements = [];

  if (empty($data['field_property'])) {
    return $replacements;
  }

  foreach ($tokens as $token => $original) {
    $list = $data[$data['field_name']];
    if (!$list instanceof SmartDateFieldItemList) {
      continue;
    }

    // Default to the first item in the list.
    $parts = explode(':', $token, 2);

    // Test for a delta as the first part.
    if (is_numeric($parts[0])) {
      if (count($parts) > 1) {
        $parts = explode(':', $parts[1], 2);
        $property_name = $parts[0];
        $format_value = $parts[1] ?? NULL;
      }
      else {
        continue;
      }
    }
    else {
      $property_name = $parts[0];
      $format_value = $parts[1] ?? NULL;
    }

    // Stop here if certain properties are not found.
    if (!in_array($property_name, ['value-closest', 'end_value-closest'])) {
      continue;
    }

    // Now parse out the pieces of the token name.
    $name_parts = explode('-', $property_name);
    $approach = array_pop($name_parts);
    $prop_needed = array_pop($name_parts);
    if (!$approach || !$prop_needed) {
      continue;
    }

    // Closest approach/fragment.
    if ($approach == 'closest') {
      if (!$format_value) {
        // This token requires a value, so skip if absent.
        continue;
      }

      // Key the values by timestamp, so they can be sorted.
      $elements = [];
      foreach ($list as $delta => $item) {
        /** @var \Drupal\smart_date\Plugin\Field\FieldType\SmartDateItem $item */

        // Drop any rows with invalid values.
        if (empty($item->value) || empty($item->end_value)) {
          continue;
        }

        // Save the original delta within the item.
        $item->delta = $delta;
        $elements[$item->value] = $item;
      }
      ksort($elements);
      $elements = array_values($elements);

      // Use the helper function to find the correct item/element instance.
      $element_index = SmartDateExtraTokensHelper::findClosestInstance($elements, $prop_needed);

      // Make sure instance is found.
      if (isset($elements[$element_index])) {
        /** @var \Drupal\smart_date\Plugin\Field\FieldType\SmartDateItem $smart_date_item */
        $smart_date_item = $elements[$element_index];
        if ($smart_date_item instanceof SmartDateItem) {
          $field_ts = NULL;

          // Make sure we have the timestamp.
          $field_ts = $smart_date_item->get($prop_needed)->getValue();
          if (!empty($field_ts)) {
            // Get timezone from smart date item.
            $field_tz = $smart_date_item->get('timezone')->getValue();
            if (empty($field_tz)) {
              $field_tz = NULL;
            }

            // Set any cache metadata.
            $bubbleable_metadata->addCacheableDependency($smart_date_item);

            // Set output/format timestamp.
            /** @var \Drupal\Core\Datetime\DateFormatter $date_formatter */
            $date_formatter = \Drupal::service('date.formatter');
            $replacements[$original] = $date_formatter->format($field_ts, '', $format_value, $field_tz);
          }
        }
      }
    }
  }

  return $replacements;
}
